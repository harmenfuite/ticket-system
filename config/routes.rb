Rails.application.routes.draw do
  get 'users/new'
  get '/layouts/application'
  get 'sessions/new'
  get 'index/home'
  get '/signup',  to: 'users#new'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  resources :users
end
